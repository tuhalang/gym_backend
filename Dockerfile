FROM adoptopenjdk/openjdk8
MAINTAINER tuhalang
COPY target/GYM.jar GYM.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar"]