package com.viettel.gym_backend.repo;

import com.viettel.gym_backend.domain.OtpToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OtpTokenRepo extends JpaRepository<OtpToken, String> {


    List<OtpToken> findByIsdnAndOtpAndTypeAndStatusOtp(String isdn, String otp, Integer type, Integer statusOtp);


    List<OtpToken> findByIsdnAndTokenAndTypeAndStatusToken(String isdn, String token, Integer type, Integer statusToken);


    List<OtpToken> findByIsdnAndRefreshTokenAndTypeAndStatusRefreshToken(String isdn, String refreshToken, Integer type, Integer statusRefreshToken);

}
