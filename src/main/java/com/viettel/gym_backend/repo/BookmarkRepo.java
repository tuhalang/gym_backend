package com.viettel.gym_backend.repo;

import com.viettel.gym_backend.domain.Bookmark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface BookmarkRepo extends JpaRepository<Bookmark, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(value= "select SYSDATE from dual", nativeQuery=true)
    Date currentDate();
}
