package com.viettel.gym_backend.repo;

import com.viettel.gym_backend.domain.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTokenRepo extends JpaRepository<UserToken, String> {

    @Query(
            value = "select u.* from user_token u where u.isdn = :isdn and u.token = :token and u.expire_time >= sysdate",
            nativeQuery = true
    )
    UserToken findByIsdnAndToken(String isdn, String token);
}
