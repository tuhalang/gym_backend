package com.viettel.gym_backend.service.impl;

import com.viettel.gym_backend.domain.Activity;
import com.viettel.gym_backend.domain.Bookmark;
import com.viettel.gym_backend.domain.UserToken;
import com.viettel.gym_backend.dto.CategoryDto;
import com.viettel.gym_backend.dto.LessonDto;
import com.viettel.gym_backend.dto.RequestDto;
import com.viettel.gym_backend.dto.ResponseDto;
import com.viettel.gym_backend.dto.TrainerDto;
import com.viettel.gym_backend.dto.UnitDto;
import com.viettel.gym_backend.repo.ActivityRepo;
import com.viettel.gym_backend.repo.BookmarkRepo;
import com.viettel.gym_backend.repo.UserTokenRepo;
import com.viettel.gym_backend.service.GymService;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
public class GymServiceImpl implements GymService {

    private static final Logger log = LoggerFactory.getLogger(GymServiceImpl.class);

    private final JdbcTemplate jdbcTemplate;

    private final BookmarkRepo bookmarkRepo;
    private final ActivityRepo activityRepo;

    private final UserTokenRepo userTokenRepo;

    private static final String WS_GET_CATEGORIES = "wsGetGymCategories";

    private static final String WS_GET_LESSON_BY_CATEGORY = "wsGetLessonByCategory";

    private static final String WS_GET_LESSON_UNITS = "wsGetLessonUnits";

    private static final String WS_BOOKMARK_UNITS = "wsBookmarkUnits";

    private static final String WS_GET_BOOKMARK_UNITS = "wsGetBookmarkUnits";

    private static final String WS_GET_TRAINER = "wsGetTrainers";

    @Value("${isValid}")
    private boolean isValid;

    public ResponseDto getInfo(RequestDto requestDto) {
        String isdn = requestDto.getIsdn();
        String token = requestDto.getToken();
        String wsCode = requestDto.getWsCode();
        LinkedHashMap<String, Object> wsRequest = requestDto.getWsRequest();
        String language = "pt";
        if (ObjectUtils.isEmpty(isdn) || ObjectUtils.isEmpty(token) || ObjectUtils.isEmpty(wsCode))
            return ResponseDto.builder()
                    .errorCode("1")
                    .message("You have not permission !")
                    .build();
        if (wsRequest.containsKey("language"))
            language = wsRequest.get("language").toString();
        if (isValid(isdn, token)) {
            if ("wsGetGymCategories".equals(wsCode))
                return getCategories(language, isdn, wsRequest);
            if ("wsGetLessonByCategory".equals(wsCode))
                return getLessonsByCategory(language, isdn, wsRequest);
            if ("wsGetLessonUnits".equals(wsCode))
                return getLessonUnits(language, isdn, wsRequest);
            if ("wsBookmarkUnits".equals(wsCode))
                return bookmarkUnits(isdn, wsRequest);
            if ("wsGetBookmarkUnits".equals(wsCode))
                return getBookmarkUnits(language, isdn, wsRequest);
            if ("wsGetTrainers".equals(wsCode))
                return getTrainers(language, isdn, wsRequest);
            if ("wsGetLatestLessons".equals(wsCode))
                return getLatestLessons(language, isdn, wsRequest);
            if ("wsGetRecentLessons".equals(wsCode))
                return getRecentLessons(language, isdn, wsRequest);
            if ("wsVisitedLesson".equals(wsCode))
                return visitedLesson(isdn, wsRequest);
        } else {
            return ResponseDto.builder()
                    .errorCode("2")
                    .message("Token is invalid")
                    .build();
        }
        return null;
    }

    private ResponseDto getRecentLessons(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "getRecentLessons";
        Integer size = 4;
        if (wsRequest.containsKey("size")) {
            size = Integer.parseInt(wsRequest.get("size").toString());
        }

        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate))
                .withFunctionName(functionName)
                .returningResultSet("lessons", BeanPropertyRowMapper.newInstance(LessonDto.class));

        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource())
                .addValue("p_size", size)
                .addValue("p_isdn", isdn)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(mapSqlParameterSource);
        List<LessonDto> lessons = (List<LessonDto>) out.get("lessons");
        ResponseDto responseDto = ResponseDto.builder().errorCode("0").message("Successfully !").onPushData("lessons", lessons).build();

        return responseDto;
    }

    private ResponseDto getLatestLessons(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "getLatestLessons";
        Integer size = 4;
        if (wsRequest.containsKey("size")) {
            size = Integer.parseInt(wsRequest.get("size").toString());
        }

        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate))
                .withFunctionName(functionName)
                .returningResultSet("lessons", BeanPropertyRowMapper.newInstance(LessonDto.class));

        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource())
                .addValue("p_size", size)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(mapSqlParameterSource);
        List<LessonDto> lessons = (List<LessonDto>) out.get("lessons");
        ResponseDto responseDto = ResponseDto.builder().errorCode("0").message("Successfully !").onPushData("lessons", lessons).build();

        return responseDto;
    }

    private ResponseDto visitedLesson(String isdn, LinkedHashMap<String, Object> wsRequest) {
        String lessonId;
        if (wsRequest.containsKey("lessonId")) {
            lessonId = wsRequest.get("lessonId").toString();
        } else {
            return ResponseDto.builder()
                    .errorCode("1")
                    .message("Missing lessonId !")
                    .build();
        }

        Activity activity = Activity.builder()
                .ID(this.activityRepo.getID())
                .isdn(isdn)
                .lessonId(lessonId)
                .createdAt(this.activityRepo.currentDate())
                .build();
        this.activityRepo.save(activity);

        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .onPushData("activity", activity)
                .build();
    }

    private ResponseDto getTrainers(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "gym_get_trainer";
        Integer page = Integer.valueOf(0);
        Integer size = Integer.valueOf(10);
        String name = "";
        String trainerId = "";
        if (wsRequest.containsKey("page"))
            page = Integer.valueOf(Integer.parseInt(wsRequest.get("page").toString()));
        if (wsRequest.containsKey("size"))
            size = Integer.valueOf(Integer.parseInt(wsRequest.get("size").toString()));
        if (wsRequest.containsKey("name"))
            name = wsRequest.get("name").toString();
        if (wsRequest.containsKey("trainerId"))
            trainerId = wsRequest.get("trainerId").toString();
        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName(functionName).returningResultSet("trainers", (RowMapper) BeanPropertyRowMapper.newInstance(TrainerDto.class));
        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource()).addValue("p_name", name).addValue("p_page", page).addValue("p_size", size).addValue("p_trainer_id", trainerId).addValue("p_language", language);
        Map<String, Object> out = simpleJdbcCall.execute((SqlParameterSource) mapSqlParameterSource);
        List<TrainerDto> trainers = (List<TrainerDto>) out.get("trainers");
        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .onPushData("trainers", trainers)
                .build();
    }

    private ResponseDto getBookmarkUnits(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "gym_get_bookmark_units";
        Integer page = Integer.valueOf(0);
        Integer size = Integer.valueOf(10);
        if (wsRequest.containsKey("page"))
            page = Integer.valueOf(Integer.parseInt(wsRequest.get("page").toString()));
        if (wsRequest.containsKey("size"))
            size = Integer.valueOf(Integer.parseInt(wsRequest.get("size").toString()));
        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName(functionName).returningResultSet("units", (RowMapper) BeanPropertyRowMapper.newInstance(UnitDto.class));
        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource()).addValue("p_isdn", isdn).addValue("p_page", page).addValue("p_size", size).addValue("p_language", language);
        Map<String, Object> out = simpleJdbcCall.execute((SqlParameterSource) mapSqlParameterSource);
        List<UnitDto> units = (List<UnitDto>) out.get("units");
        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .onPushData("units", units)
                .build();
    }

    private ResponseDto bookmarkUnits(String isdn, LinkedHashMap<String, Object> wsRequest) {
        String unitId = "";
        if (wsRequest.containsKey("unitId")) {
            unitId = wsRequest.get("unitId").toString();
        } else {
            return ResponseDto.builder()
                    .errorCode("1")
                    .message("The record(s) is not exists !")
                    .build();
        }
        Bookmark bookmark = Bookmark.builder().ID(this.bookmarkRepo.getID()).isdn(isdn).unitId(unitId).createdAt(this.bookmarkRepo.currentDate()).build();
        this.bookmarkRepo.save(bookmark);
        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .build();
    }

    private ResponseDto getLessonUnits(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "gym_get_units";
        String lessonId = "";
        if (wsRequest.containsKey("lessonId")) {
            lessonId = wsRequest.get("lessonId").toString();
        } else {
            return ResponseDto.builder()
                    .errorCode("1")
                    .message("The record(s) is not exists !")
                    .build();
        }
        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName(functionName).returningResultSet("units", (RowMapper) BeanPropertyRowMapper.newInstance(UnitDto.class));
        MapSqlParameterSource mapSqlParameterSource1 = (new MapSqlParameterSource()).addValue("p_lesson_id", lessonId).addValue("p_language", language);
        Map<String, Object> out = simpleJdbcCall.execute((SqlParameterSource) mapSqlParameterSource1);
        List<UnitDto> units = (List<UnitDto>) out.get("units");
        SimpleJdbcCall simpleJdbcCall1 = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName("gym_get_lessons_by_id").returningResultSet("lessons", (RowMapper) BeanPropertyRowMapper.newInstance(LessonDto.class));
        MapSqlParameterSource mapSqlParameterSource2 = (new MapSqlParameterSource()).addValue("p_id", lessonId).addValue("p_language", language);
        Map<String, Object> out1 = simpleJdbcCall1.execute((SqlParameterSource) mapSqlParameterSource2);
        List<LessonDto> lessons = (List<LessonDto>) out1.get("lessons");
        LessonDto lessonDto = null;
        if (lessons.size() > 0)
            lessonDto = lessons.get(0);
        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .onPushData("units", units)
                .onPushData("lesson", lessonDto)
                .build();
    }

    private ResponseDto getLessonsByCategory(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "gym_get_lessons";
        Integer page = Integer.valueOf(0);
        Integer size = Integer.valueOf(10);
        String categoryId = "";
        String trainerId = "";
        if (wsRequest.containsKey("page"))
            page = Integer.valueOf(Integer.parseInt(wsRequest.get("page").toString()));
        if (wsRequest.containsKey("size"))
            size = Integer.valueOf(Integer.parseInt(wsRequest.get("size").toString()));
        if (wsRequest.containsKey("categoryId")) {
            categoryId = wsRequest.get("categoryId").toString();
        }

        if (wsRequest.containsKey("trainerId"))
            trainerId = wsRequest.get("trainerId").toString();
        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate))
                .withFunctionName(functionName)
                .returningResultSet("lessons", (RowMapper) BeanPropertyRowMapper.newInstance(LessonDto.class));
        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource())
                .addValue("p_page", page)
                .addValue("p_size", size)
                .addValue("p_parent_id", categoryId)
                .addValue("p_trainer_id", trainerId).addValue("p_language", language);
        Map<String, Object> out = simpleJdbcCall.execute((SqlParameterSource) mapSqlParameterSource);
        List<LessonDto> lessons = (List<LessonDto>) out.get("lessons");
        ResponseDto responseDto = ResponseDto.builder().errorCode("0").message("Successfully !").onPushData("lessons", lessons).build();
        if (categoryId != null) {
            SimpleJdbcCall simpleJdbcCall1 = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName("gym_get_category_by_id").returningResultSet("category", (RowMapper) BeanPropertyRowMapper.newInstance(CategoryDto.class));
            MapSqlParameterSource mapSqlParameterSource1 = (new MapSqlParameterSource()).addValue("p_category_id", categoryId).addValue("p_language", language);
            Map<String, Object> out1 = simpleJdbcCall1.execute((SqlParameterSource) mapSqlParameterSource1);
            List<CategoryDto> categories = (List<CategoryDto>) out1.get("category");
            if (categories != null && categories.size() > 0)
                responseDto.pushData("category", categories.get(0));
        }
        return responseDto;
    }

    private ResponseDto getCategories(String language, String isdn, LinkedHashMap<String, Object> wsRequest) {
        String functionName = "gym_get_categories";
        Integer page = Integer.valueOf(0);
        Integer size = Integer.valueOf(10);
        String parentId = "";
        String trainerId = "";
        if (wsRequest.containsKey("page"))
            page = Integer.valueOf(Integer.parseInt(wsRequest.get("page").toString()));
        if (wsRequest.containsKey("size"))
            size = Integer.valueOf(Integer.parseInt(wsRequest.get("size").toString()));
        if (wsRequest.containsKey("parentId"))
            parentId = wsRequest.get("parentId").toString();
        if (wsRequest.containsKey("trainerId"))
            trainerId = wsRequest.get("trainerId").toString();
        SimpleJdbcCall simpleJdbcCall = (new SimpleJdbcCall(this.jdbcTemplate)).withFunctionName(functionName).returningResultSet("categories", (RowMapper) BeanPropertyRowMapper.newInstance(CategoryDto.class));
        MapSqlParameterSource mapSqlParameterSource = (new MapSqlParameterSource()).addValue("p_page", page).addValue("p_size", size).addValue("p_parent_id", parentId).addValue("p_trainer_id", trainerId).addValue("p_language", language);
        Map<String, Object> out = simpleJdbcCall.execute((SqlParameterSource) mapSqlParameterSource);
        List<CategoryDto> categories = (List<CategoryDto>) out.get("categories");
        return ResponseDto.builder()
                .errorCode("0")
                .message("Successfully !")
                .onPushData("categories", categories)
                .build();
    }

    private boolean isValid(String isdn, String token) {
        if (ObjectUtils.isEmpty(Boolean.valueOf(isValid)) || isValid) {
            UserToken userToken = userTokenRepo.findByIsdnAndToken(isdn, token);
            if (ObjectUtils.isEmpty(userToken))
                return false;
        }
        return true;
    }
}
