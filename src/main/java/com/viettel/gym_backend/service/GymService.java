package com.viettel.gym_backend.service;

import com.viettel.gym_backend.dto.RequestDto;
import com.viettel.gym_backend.dto.ResponseDto;
import com.viettel.gym_backend.dto.WrapperRequest;
import com.viettel.gym_backend.dto.WrapperResponse;

public interface GymService {
    ResponseDto getInfo(RequestDto requestDto);
}
