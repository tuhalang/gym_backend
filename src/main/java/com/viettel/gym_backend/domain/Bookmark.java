package com.viettel.gym_backend.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "bookmark")
public class Bookmark {

    @Id
    @Column(name = "id")
    private String ID;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "unit_id")
    private String unitId;
    @Column(name = "CREATED_AT")
    private Date createdAt;
}
