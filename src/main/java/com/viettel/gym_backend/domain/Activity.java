package com.viettel.gym_backend.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "activity")
public class Activity {
    @Id
    @Column(name = "id")
    private String ID;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "lesson_id")
    private String lessonId;
    @Column(name = "CREATED_AT")
    private Date createdAt;
}
