package com.viettel.gym_backend.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "USER_TOKEN")
public class UserToken {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "ISDN")
    private String isdn;
    @Column(name = "EXPIRE_TIME")
    @Temporal(TemporalType.DATE)
    private Date expireTime;
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;
    @Column(name = "REFRESH_EXPIRE_TIME")
    @Temporal(TemporalType.DATE)
    private Date refreshExpireTime;
}
