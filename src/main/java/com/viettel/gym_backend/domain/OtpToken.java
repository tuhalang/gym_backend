package com.viettel.gym_backend.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "OTP_TOKEN")
public class OtpToken {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "ISDN")
    private String isdn;
    @Column(name = "CREATED_AT")
    private Date createdAt;
    @Column(name = "STATUS_TOKEN")
    private Integer statusToken;
    @Column(name = "STATUS_REFRESH_TOKEN")
    private Integer statusRefreshToken;
    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "OTP")
    private String otp;
    @Column(name = "STATUS_OTP")
    private Integer statusOtp;
    @Column(name = "TYPE")
    private Integer type;

}

