package com.viettel.gym_backend.controller;


import com.viettel.gym_backend.dto.RequestDto;
import com.viettel.gym_backend.dto.ResponseDto;
import com.viettel.gym_backend.dto.WrapperRequest;
import com.viettel.gym_backend.dto.WrapperResponse;
import com.viettel.gym_backend.service.GymService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class GymController {

    private final GymService gymService;

    @RequestMapping(value = "/gym", method = RequestMethod.POST)
    public ResponseDto getInfo(@RequestBody RequestDto wrapperRequest){
        return gymService.getInfo(wrapperRequest);

    }
}
