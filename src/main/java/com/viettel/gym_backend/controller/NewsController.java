package com.viettel.gym_backend.controller;

import com.viettel.gym_backend.domain.OtpToken;
import com.viettel.gym_backend.dto.RequestDto;
import com.viettel.gym_backend.dto.ResponseDto;
import com.viettel.gym_backend.dto.WrapperRequest;
import com.viettel.gym_backend.dto.WrapperResponse;
import com.viettel.gym_backend.repo.OtpTokenRepo;
import com.viettel.gym_backend.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class NewsController {


    private final OtpTokenRepo otpTokenRepo;

//    @RequestMapping(value = "/news", method = RequestMethod.GET)
//    public ResponseDto getLatestNews(@RequestParam Integer page,
//                                     @RequestParam Integer size,
//                                     @RequestParam String language,
//                                     @RequestParam(required = false) String categoryId,
//                                     @RequestParam(required = false) String title){
//        return newsService.fetch(page, size, language, title, categoryId);
//    }

    @RequestMapping(value = "/news", method = RequestMethod.POST)
    public WrapperResponse getInfo(@RequestBody WrapperRequest wrapperRequest){
        if("wsGetOtpNewsService".equals(wrapperRequest.getWsCode())){

            RequestDto requestDto = wrapperRequest.getWsRequest();
            //
            String otp = OTP(6);
            //
            OtpToken otpToken = OtpToken.builder()
                    .id(UUID.randomUUID().toString())
                    .otp(otp)
                    .isdn(requestDto.getIsdn())
                    .type(1)
                    .statusOtp(1)
                    .createdAt(new Date())
                    .build();
            otpTokenRepo.save(otpToken);
            //
            ResponseDto responseDto = ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_OK)
                    .build();
            return WrapperResponse.builder()
                    .errorCode("S200")
                    .result(responseDto)
                    .build();
        }else if("wsLoginOtpNewsService".equals(wrapperRequest.getWsCode())){
            RequestDto requestDto = wrapperRequest.getWsRequest();

//            List<OtpToken> otpTokens = otpTokenRepo.findByIsdnAndOtpAndTypeAndStatusOtp(requestDto.getIsdn(), requestDto.getOtp(), 1, 1);
//            if(otpTokens != null && otpTokens.size() > 0){
                String token = UUID.randomUUID().toString();
                String refreshToken = UUID.randomUUID().toString();
                Boolean isRegister = true;
                if(requestDto.getIsdn().startsWith("1")) {
                    isRegister = false;
                    token = null;
                    refreshToken = null;
                }

                if(isRegister){
                    OtpToken otpToken = OtpToken.builder()
                            .id(UUID.randomUUID().toString())
                            .token(token)
                            .refreshToken(refreshToken)
                            .isdn(requestDto.getIsdn())
                            .statusToken(1)
                            .statusRefreshToken(1)
                            .type(2)
                            .createdAt(new Date())
                            .build();
                    otpTokenRepo.save(otpToken);
                }

                ResponseDto responseDto = ResponseDto.builder()
                        .errorCode(Constant.ERROR_CODE_OK)
                        .onPushData("token", token)
                        .onPushData("refreshToken", refreshToken)
                        .onPushData("isRegister", isRegister)
                        .build();

                return WrapperResponse.builder()
                        .errorCode("S200")
                        .result(responseDto)
                        .build();
//            }
//
//            ResponseDto responseDto = ResponseDto.builder()
//                    .errorCode(Constant.ERROR_CODE_NOK)
//                    .message("Wrong otp")
//                    .build();
//
//            return WrapperResponse.builder()
//                    .errorCode("S200")
//                    .result(responseDto)
//                    .build();
        }else if("wsRefreshTokenNewsService".equals(wrapperRequest.getWsCode())){

            RequestDto requestDto = wrapperRequest.getWsRequest();
            List<OtpToken> otpTokens = otpTokenRepo.findByIsdnAndRefreshTokenAndTypeAndStatusRefreshToken(requestDto.getIsdn(), requestDto.getRefreshToken(), 2, 1);
            if(otpTokens != null && otpTokens.size() > 0){
                otpTokens.forEach(e -> {
                    e.setStatusToken(1);
                });
            }
            otpTokenRepo.saveAll(otpTokens);

            ResponseDto responseDto = ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_OK)
                    .build();
            return WrapperResponse.builder()
                    .errorCode("S200")
                    .result(responseDto)
                    .build();

        }else if("wsRegisterNewsService".equals(wrapperRequest.getWsCode())){

            ResponseDto responseDto = ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_OK)
                    .build();
            return WrapperResponse.builder()
                    .errorCode("S200")
                    .result(responseDto)
                    .build();

        }else if("wsCancelNewsService".equals(wrapperRequest.getWsCode())){

            ResponseDto responseDto = ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_OK)
                    .build();
            return WrapperResponse.builder()
                    .errorCode("S200")
                    .result(responseDto)
                    .build();

        }

        return WrapperResponse.builder()
                .errorCode("S200")
                .build();
    }

    static String OTP(int len)
    {
        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new SecureRandom();

        char[] otp = new char[len];

        for (int i = 0; i < len; i++)
        {
            otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otp);
    }
}
