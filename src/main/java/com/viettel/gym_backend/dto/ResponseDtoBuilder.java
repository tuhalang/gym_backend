package com.viettel.gym_backend.dto;


import com.viettel.gym_backend.utils.Constant;

import java.util.LinkedHashMap;

public class ResponseDtoBuilder {
    private String errorCode = Constant.ERROR_CODE_NOK;
    private String message;
    private LinkedHashMap<String, Object> data;

    public ResponseDtoBuilder errorCode(String errorCode){
        this.errorCode = errorCode;
        return this;
    }

    public ResponseDtoBuilder message(String message){
        this.message = message;
        return this;
    }

    public ResponseDtoBuilder data(LinkedHashMap<String, Object> data){
        this.data = data;
        return this;
    }

    public ResponseDtoBuilder onPushData(String key, Object value){
        if(this.data == null){
            this.data = new LinkedHashMap<>();
        }
        this.data.put(key, value);
        return this;
    }

    public ResponseDto build(){
        return new ResponseDto(this.errorCode, this.message, this.data);
    }
}
