package com.viettel.gym_backend.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryDto {

    private String id;
    private String parentId;
    private String trainerId;
    private String name;
    private String description;
    private String imageUrl;

    @Transient
    private List<CategoryDto> subCategories;
}
