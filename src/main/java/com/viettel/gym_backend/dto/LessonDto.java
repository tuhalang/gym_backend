package com.viettel.gym_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonDto {

    private String id;
    private String categoryId;
    private String categoryName;
    private String trainerId;
    private String lessonLever;
    private String intensity;
    private String name;
    private String content;
    private String description;
    private Integer estimateTime;
    private String imageUrl;
}
