package com.viettel.gym_backend.dto;

import com.viettel.gym_backend.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto {

    private String errorCode = Constant.ERROR_CODE_NOK;
    private String message;
    private LinkedHashMap<String, Object> wsResponse;

    public void pushData(String key, Object value){
        if(this.wsResponse == null){
            this.wsResponse = new LinkedHashMap<>();
        }
        this.wsResponse.put(key, value);
    }

    public static ResponseDtoBuilder builder(){
        return new ResponseDtoBuilder();
    }
}
