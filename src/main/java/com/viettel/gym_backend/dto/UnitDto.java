package com.viettel.gym_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UnitDto {
    private String id;
    private String lessonId;
    private String name;
    private String videoUrl;
    private String content;
    private Integer estimateTime;

}
