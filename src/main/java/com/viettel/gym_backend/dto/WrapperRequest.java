package com.viettel.gym_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WrapperRequest {
    private String isWrap;
    private String wsCode;
    private RequestDto wsRequest;
}
