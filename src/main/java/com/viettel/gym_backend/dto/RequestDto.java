package com.viettel.gym_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestDto {

    private String isdn;
    private String otp;
    private String refreshToken;
    private String otpType;
    private String transId;
    private String gameCode;
    private String language;
    private String wsCode;
    private String token;
    private LinkedHashMap<String, Object> wsRequest;
}
